package rechteck;

import java.awt.Point;

public class Rechteck {
	private double Width;
	private double Height;
	private Point Koordianten;
    
	public Rechteck(double width, double height, Point koordianten)
	{
	  setKoordianten(koordianten);
	  setWidth(width);
	  setHeight(height);
	}
	
	public void setWidth(double width) {
		if(width > 0)
			this.Width = width;
		else
			this.Width = 0;
	}
	
	public void setHeight(double height) {
		if(height > 0)
			this.Height = height;
		else
			this.Height = 0;
	}
	
	public double getWidth() {
		return this.Width;
	}
	
	public double getHeight() {
		return this.Height;
	}
	
	public double getDurchmesser() {
		return Math.sqrt(Math.pow(this.Width, 2) * Math.pow(this.Height, 2));
	}
	
	public double getFlaeche() {
		return Width * Height;
	}
	
	public double getUmfang() {
		return 2 * Width + 2 * Height;
	}

	public void setKoordianten(Point koordinaten) {
		this.Koordianten = koordinaten; 
	}
	
	public Point getKoordianten() {
		return this.Koordianten;
	}
}
