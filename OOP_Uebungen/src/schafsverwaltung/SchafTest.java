package schafsverwaltung;

public class SchafTest {
	public static void main(String[] args) {
		System.out.println("\n--- Instanziere objekt1 ---");
		Schaf schaf1 = new Schaf();
		System.out.println("\n--- Instanziere objekt2 ---");
		Schaf schaf2 = new Schaf("Othello");
		System.out.println("\n--- Instanziere objekt3 ---");
		Schaf schaf3 = new Schaf("Cloud", (byte) 4, 2.15F);
		System.out.println();
		schaf1.merkmaleAusgeben("objekt1");
		schaf2.merkmaleAusgeben("objekt2");
		schaf3.merkmaleAusgeben("objekt3");
	}
}
