package schafsverwaltung;

public class Schaf {

	String name;
	byte alter;
	float wollMenge;

	public Schaf() {
		System.out.println("Im parameterlosen Konstruktor 1.");
	}

	public Schaf(String name) {
		this.name = name;
		System.out.println("Im Konstruktor 2.");
	}

	public Schaf(String name, byte alter, float wolle) {
		this(name);
		this.alter = alter;

		wollMenge = wolle;
		System.out.println("Im Konstruktor 3.");
	}
	
	public void merkmaleAusgeben(String objektName) {
		System.out.println("\n- Ausgabe der Merkmale von " + objektName + " -");
		System.out.println("Name: " + name);
		System.out.println("Alter: " + alter + " Jahre");
		System.out.println("Wollmenge: " + wollMenge + " m^2");
	}
}
