package angestelltenverwaltung;

public class AngestellterTest {
	
	public static void main(String[] args) {
		Angestellter ang1 = new Angestellter();
		Angestellter ang2 = new Angestellter("Petersen");
		Angestellter ang3 = new Angestellter("Schmidt", 8000);
		
		ang1.setName("Meier");
		ang1.setGehalt(4500);
		ang2.setGehalt(6000);
		
		System.out.println("Name: " + ang1.getName());
		System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
		System.out.println("\nName: " + ang2.getName());
		System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
		System.out.println("\nName: " + ang3.getName());
		System.out.println("Gehalt: " + ang3.getGehalt() + " Euro");
	}
}
