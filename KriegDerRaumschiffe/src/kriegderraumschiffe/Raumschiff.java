package kriegderraumschiffe;

import java.util.ArrayList;
import java.util.List;

public class Raumschiff {
	
	int PhotonenTorpedoAnzahl;
	int EnergieversorgungInProzent;
	int SchildeInProzent;
	int HuelleInProzent;
	int LebenserhaltungssystemeInProzent;
	int AndroidenAnzahl;
	String Schiffsname;
	ArrayList<String> BroadcastKommunikator;
	ArrayList<Ladung> Ladungsverzeichnis;
	
	/**
	 * Erzeugt ein Objekt der Klasse Raumschiff
	 * @param photonenTorpedoAnzahl
	 * @param energieversorgungInProzent
	 * @param schildeInProzent
	 * @param huelleInProzent
	 * @param lebenserhaltungssystemeInProzent
	 * @param androidenAnzahl
	 * @param schiffsname
	 * @param broadcastKommunikator
	 * @param ladungsverzeichnis
	 */
	public Raumschiff(int photonenTorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname,
			ArrayList<String> broadcastKommunikator, ArrayList<Ladung> ladungsverzeichnis) {
		PhotonenTorpedoAnzahl = photonenTorpedoAnzahl;
		EnergieversorgungInProzent = energieversorgungInProzent;
		SchildeInProzent = schildeInProzent;
		HuelleInProzent = huelleInProzent;
		LebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		AndroidenAnzahl = androidenAnzahl;
		Schiffsname = schiffsname;
		BroadcastKommunikator = broadcastKommunikator;
		Ladungsverzeichnis = ladungsverzeichnis;
	}

	/**
	 * Schie�t ein Photonentorpedo ab und trifft das Ziel wenn Torpedos vorhanden sind
	 * @param ziel
	 */
	public void photonentorpedosAbschiessen(Raumschiff ziel) {
		if(this.PhotonenTorpedoAnzahl < 1) {
			System.out.println("-=*Click*=-");
		} else {
			this.PhotonenTorpedoAnzahl -= 1;
			System.out.println("Photonentorpedo abgeschossen");
			trefferVermerken(ziel);
		}
	}
	
	/**
	 * Schie�t die Phaserkanonen ab und trifft das Ziel
	 * @param ziel
	 */
	public void phaserkanonenAbschiessen(Raumschiff ziel) {
		if(this.EnergieversorgungInProzent < 50) {
			System.out.println("-=*Click*=-");
		} else {
			this.EnergieversorgungInProzent -= 50;
			System.out.println("Phaserkanone abgeschossen");
			trefferVermerken(ziel);
		}
	}
	
	/**
	 * Vermerkt einen Treffer und sch�digt die Schilde, H�lle und Energieversorgun des Ziels
	 * @param ziel
	 */
	private void trefferVermerken(Raumschiff ziel) {
		System.out.println(ziel.getSchiffsname() + "wurde getroffen!");
		ziel.setSchildeInProzent(ziel.getSchildeInProzent() - 50);
		
		if(ziel.getSchildeInProzent() < 1) {
			ziel.setHuelleInProzent(ziel.getHuelleInProzent() - 50);
			ziel.setEnergieversorgungInProzent(ziel.getEnergieversorgungInProzent() - 50);
			
			if(ziel.getHuelleInProzent() < 1) {
				ziel.nachrichtenSenden("Lebenserhaltungssysteme vollst�ndig zerst�rt.");
				ziel.setLebenserhaltungssystemeInProzent(0);
			}
		}
	}
	
	/**
	 * Sendet eine Nachricht an den Broadcast Kommunikator des Schiffs
	 * @param nachricht
	 */
	public void nachrichtenSenden(String nachricht) {
		this.BroadcastKommunikator.add(nachricht);
		System.out.println(nachricht);
	}
	
	/**
	 * f�gt eine Ladung zum Ladungsverzeichnis hinzu
	 * @param aufzuladeneLadung
	 */
	public void ladungenBeladen(Ladung aufzuladeneLadung) {
		this.Ladungsverzeichnis.add(aufzuladeneLadung);
	}
	
	/**
	 * GIbt die Nachrichten aus dem Broadcastkommunikator zur�ck
	 * @return Liste mit Nachrichten aus Broadcastkommunikator
	 */
	public List<String> logbucheintr�geZur�ckgeben() {
		return this.BroadcastKommunikator;
	}
	
	/**
	 * l�dt neue Photonentorpedos
	 * @param anzahlTorpedos
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		
	}
	
	/**
	 * sendet Reparaturandroiden aus und l�sst die Schutzschilde, ENergieversorgung, oder H�lle reparieren
	 * @param schutzschilde
	 * @param energieversorgung
	 * @param schiffshuelle
	 * @param anzahlAndroide
	 */
	public void reparaturAndroidenSenden(Boolean schutzschilde, 
			Boolean energieversorgung, Boolean schiffshuelle, int anzahlAndroide) {
		
	}
	
	/**
	 * Gibt den aktuellen Zustand des Schiffs an die Konsole aus
	 */
	public void raumschiffZustandAusgeben() {
		System.out.println("Photonentorpedos: " + getPhotonenTorpedoAnzahl());
		System.out.println("Energieversorgung: " + getEnergieversorgungInProzent() + "%");
		System.out.println("Schilde: " + getSchildeInProzent() + "%");
		System.out.println("H�lle: " + getHuelleInProzent() + "%");
		System.out.println("Lebenserhaltungssysteme: " + getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("Androiden: " + getAndroidenAnzahl());
		System.out.println("Schiffsname: " + getSchiffsname());
		System.out.println("Broadcastkommunikator: " + getBroadcastKommunikator());
		System.out.println("Ladungsverzeichnis: " + getLadungsverzeichnis());
	}
	
	/**
	 * f�gt eine neue Ladung zum Ladungsverzeichnis hinzu
	 * @param neueLadung
	 */
	public void addLadung(Ladung neueLadung) {
		this.Ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Gibt das Ladungsverzeichnis an die Konsole zur�ck
	 */
	public void ladungsverzeichnisAusgeben() {
		for(int i = 0; i < this.Ladungsverzeichnis.size(); i++) {
			System.out.println(this.Ladungsverzeichnis.get(i).Bezeichnung + ": " + this.Ladungsverzeichnis.get(i).Menge);
		}
	}

	/**
	 * Gibt den Wert f�r diePhotonentorpedos zur�ck
	 * @return Anzahl Photonentorpedos
	 */
	public int getPhotonenTorpedoAnzahl() {
		return PhotonenTorpedoAnzahl;
	}

	/**
	 * Setzt den Wert f�r die Photonentorpedos
	 * @param photonenTorpedoAnzahl
	 */
	public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
		PhotonenTorpedoAnzahl = photonenTorpedoAnzahl;
	}
	
	/**
	 * Gibt den Wert f�r die Energieversorgung zur�ck
	 * @return Energieversorgung In Prozent
	 */
	public int getEnergieversorgungInProzent() {
		return EnergieversorgungInProzent;
	}
	
	/**
	 * Setzt den Wert f�r die Energieversorgung
	 * @param energieversorgungInProzent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		EnergieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * Gibt den Wert f�r die Schilde zur�ck
	 * @return Schilde In Prozent
	 */
	public int getSchildeInProzent() {
		return SchildeInProzent;
	}

	/**
	 * Setzt den Wert f�r die Schilde
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		SchildeInProzent = schildeInProzent;
	}
	
	/**
	 * Gibt den Wert f�r die H�lle zur�ck
	 * @return H�lle In Prozent
	 */
	public int getHuelleInProzent() {
		return HuelleInProzent;
	}

	/**
	 * Setzt den Wert f�r die H�lle
	 * @param huelleInProzent
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		HuelleInProzent = huelleInProzent;
	}

	/**
	 * Gibt den Wert f�r die Lebenserhaltungssysteme zur�ck
	 * @return Lebenserhaltungssysteme In Prozent
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return LebenserhaltungssystemeInProzent;
	}

	/**
	 * Setzt den Wert f�r die Lebenserhaltungssysteme
	 * @param lebenserhaltungssystemeInProzent
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		LebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	/**
	 * Gibt den Wert f�r die Androiden Anzahl zur�ck
	 * @return Anzahl Androide
	 */
	public int getAndroidenAnzahl() {
		return AndroidenAnzahl;
	}

	/**
	 * Setzt den Wert f�r die Androidenanzahl
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		AndroidenAnzahl = androidenAnzahl;
	}

	/**
	 * Gibt den Wert f�r den Schiffsnamen zur�ck
	 * @return Schiffsname
	 */
	public String getSchiffsname() {
		return Schiffsname;
	}

	/**
	 * Setzt den Wert f�r den Schiffsnamen
	 * @param schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		Schiffsname = schiffsname;
	}

	/**
	 * Gibt den Wert f�r den Broadcastkommunikator zur�ck
	 * @return Liste mit Nachrichten
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return BroadcastKommunikator;
	}

	/**
	 * Setzt den Wert f�r den Broadcastkommunikator
	 * @param broadcastKommunikator
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		BroadcastKommunikator = broadcastKommunikator;
	}
	
	/**
	 * Gibt den Wert f�r das Ladungsverzeichnis zur�ck
	 * @return Liste mit Ladungen
	 */
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return Ladungsverzeichnis;
	}

	/**
	 * Setzt den Wert f�r das Ladungsverzeichnis
	 * @param ladungsverzeichnis
	 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		Ladungsverzeichnis = ladungsverzeichnis;
	}
}
