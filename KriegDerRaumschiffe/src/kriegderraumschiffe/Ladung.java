package kriegderraumschiffe;

public class Ladung {
	
	String Bezeichnung;
	int Menge;
	
	/**
	 * Generiert ein Ladungs-Objekt, welche die Attribute Bezeichnung und Menge hat
	 * @author Justin Buhlemann
	 * @param bezeichnung
	 * @param menge
	 */
	public Ladung(String bezeichnung, int menge) {
		Bezeichnung = bezeichnung;
		Menge = menge;
	}

	/**
	 * gibt die Bezeichnung der Ladung zur�ck
	 * @return String
	 */
	public String getBezeichnung() {
		return Bezeichnung;
	}

	/**
	 * setzt die Bezeichnung der Ladung
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		Bezeichnung = bezeichnung;
	}

	/**
	 * gibt die Menge der Ladung zur�ck
	 * @return Menge
	 */
	public int getMenge() {
		return Menge;
	}

	/**
	 * setzt die Menge der Ladung
	 * @param menge
	 */
	public void setMenge(int menge) {
		Menge = menge;
	}
	

}
