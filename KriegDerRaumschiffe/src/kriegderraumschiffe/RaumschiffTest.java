package kriegderraumschiffe;

import java.util.ArrayList;

public class RaumschiffTest {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta", new ArrayList<String>(), new ArrayList<Ladung>());
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara", new ArrayList<String>(), new ArrayList<Ladung>());
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var", new ArrayList<String>(), new ArrayList<Ladung>());
		Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladung2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung ladung3 = new Ladung("Borg-Schrott", 5);
		Ladung ladung4 = new Ladung("Rote Materie", 2);
		Ladung ladung5 = new Ladung("Plasma-Waffe", 50);
		Ladung ladung6 = new Ladung("Forschungssonde", 35);
		Ladung ladung7 = new Ladung("Photonentorpedo", 3);
		klingonen.addLadung(ladung1);
		klingonen.addLadung(ladung2);
		romulaner.addLadung(ladung3);
		romulaner.addLadung(ladung4);
		romulaner.addLadung(ladung5);
		vulkanier.addLadung(ladung6);
		vulkanier.addLadung(ladung7);
		
		klingonen.photonentorpedosAbschiessen(romulaner);
		romulaner.phaserkanonenAbschiessen(klingonen);
		klingonen.nachrichtenSenden("Gewalt ist nicht logisch!");
		klingonen.raumschiffZustandAusgeben();
		klingonen.ladungsverzeichnisAusgeben();
		klingonen.photonentorpedosAbschiessen(romulaner);
		klingonen.photonentorpedosAbschiessen(romulaner);
		klingonen.raumschiffZustandAusgeben();
		romulaner.raumschiffZustandAusgeben();
		vulkanier.raumschiffZustandAusgeben();
	}
}
